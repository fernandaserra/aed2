#include"arn.h"
#include<stdio.h>
#include<stdlib.h>

tipoNo* criar()
{
	return NULL;
}

tipoNo* inserir(tipoNo *r, int chave)
{
    if(r != NULL)
    {
			r->cor = 0;
			if(chave > r->chave) r->dir = inserir(r->dir, chave);
			else r->esq = inserir(r->esq, chave);
			balancear(r);
    }else
    {
    		r = (tipoNo *)malloc(sizeof(tipoNo));
    		r->chave = chave; r->cor = 1; //raiz é preta
    		r->dir = NULL; r->esq = NULL; r->pai = NULL;
    }
    return r;
}

tipoNo* trocarCor(tipoNo *r)
{
	r->cor = !r->cor;
	if (r->esq) r->esq->cor = !r->esq->cor;
	else if (r->dir) r->dir->cor = !r->dir->cor;

	if(r->cor == 0) r->cor = 1;
	return r;
}

tipoNo* rotacionarDireita(tipoNo *r)
{
    tipoNo *aux = r, *aux2 = aux->dir;
    aux->dir = aux2->esq;
    aux2->esq = aux;

    return aux2;
}

tipoNo* rotacionarEsquerda(tipoNo *r)
{
  tipoNo *aux = r, *aux2 = aux->esq;
  aux->esq = aux2->dir;
  aux2->dir = aux;

  return aux2;
}

tipoNo* rotacionar2xDireita(tipoNo *r)
{
  r->dir = rotacionarEsquerda(r->dir);
  return rotacionarDireita(r);
}

tipoNo* rotacionar2xEsquerda(tipoNo *r)
{
  r->esq = rotacionarDireita(r->esq);
  return rotacionarEsquerda(r);
}

tipoNo* balancear(tipoNo *r)
{
	if(r)
	{
		tipoNo *aux = r;
		if(r->pai->cor == 0 && r->cor == 0) trocarCor(aux);

	}

	return r;
}
