typedef struct tipoNo
{
	int chave, cor;
	struct tipoNo *esq, *dir, *pai;
} tipoNo;

tipoNo* criar();

tipoNo* inserir(tipoNo *r, int chave);

tipoNo* trocarCor(tipoNo *r);

tipoNo* rotacionarDireita(tipoNo *r);

tipoNo* rotacionarEsquerda(tipoNo *r);

tipoNo* rotacionar2xDireita(tipoNo *r);

tipoNo* rotacionar2xEsquerda(tipoNo *r);

tipoNo* balancear(tipoNo *r);
