typedef struct tipoNo
{
  int chave, alt;
  struct tipoNo *esq, *dir; //apontadores da esquerda e direita
} tipoNo;

tipoNo* criar();

tipoNo* inserir(tipoNo *r, int chave);

tipoNo* inserirBalanceada(tipoNo *r, int chave);

int altura(tipoNo *r);

int fatorBalanceamento(tipoNo *r);

tipoNo* rotacionarDireita(tipoNo *r);

tipoNo* rotacionarEsquerda(tipoNo *r);

tipoNo* rotacionar2xDireita(tipoNo *r);

tipoNo* rotacionar2xEsquerda(tipoNo *r);

tipoNo* balancear(tipoNo *r);

tipoNo* buscaAVL(tipoNo *r, int chave);
