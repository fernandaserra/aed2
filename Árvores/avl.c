#include"avl.h"
#include<stdio.h>
#include<stdlib.h>

tipoNo* criar()
{
  return NULL;
}

tipoNo* inserir(tipoNo *r, int chave)
{
    if(r != NULL)
    {
			if(chave > r->chave) r->dir = inserir(r->dir, chave);
			else r->esq = inserir(r->esq, chave);
    }else
    {
    		r = (tipoNo *)malloc(sizeof(tipoNo));
    		r->chave = chave;
    		r->dir = NULL; r->esq = NULL;
    }
    return r;
}

tipoNo* inserirBalanceada(tipoNo *r, int chave)
{
  r = inserir(r, chave);
  if(r) return balancear(r);

  return r;
}

int altura(tipoNo *r)
{
  int hesq = 0, hdir = 0;
  if(r)
  {
    if(r->esq) hesq = altura(r->esq);
    if(r->dir) hdir = altura(r->dir);

    if(hdir > hesq) return hdir + 1;
    else return hesq + 1;
  }else return (-1);
}

int fatorBalanceamento(tipoNo *r)
{
  return (altura(r->dir) - altura(r->esq));
}

tipoNo* rotacionarDireita(tipoNo *r)
{
    tipoNo *pai = r, *filhoDir = pai->dir;
    pai->dir = filhoDir->esq;
    filhoDir->esq = pai;

    return filhoDir;
}

tipoNo* rotacionarEsquerda(tipoNo *r)
{
  tipoNo *pai = r, *filhoEsq = pai->esq;
  pai->esq = filhoEsq->dir;
  filhoEsq->dir = pai;

  return filhoEsq;
}

tipoNo* rotacionar2xDireita(tipoNo *r)
{
  r->dir = rotacionarEsquerda(r->dir);
  return rotacionarDireita(r);
}

tipoNo* rotacionar2xEsquerda(tipoNo *r)
{
  r->esq = rotacionarDireita(r->esq);
  return rotacionarEsquerda(r);
}

tipoNo* balancear(tipoNo *r)
{
  tipoNo *rFinal = criar(); int f = fatorBalanceamento(r);

  if(f >= 2) //dir maior
  {
    if (fatorBalanceamento(r->dir) > 1) rFinal = rotacionar2xDireita(r);
    else rFinal = rotacionarDireita(r);
  } else if (f <= -2) //esq maior
  {
    if (fatorBalanceamento(r->esq) < -1) rFinal = rotacionar2xEsquerda(r);
    else rFinal = rotacionarEsquerda(r);
  } else rFinal = r; // não precisa balancear

  return rFinal;
}

tipoNo* buscaAVL(tipoNo *r, int chave)
{
		if (r == NULL) return NULL;
		else if (r->chave == chave) return r;
		else if ((r->chave) > chave) return (buscaAVL(r->esq, chave));
		else if (r->chave < chave) return (buscaAVL(r->dir, chave));
}
