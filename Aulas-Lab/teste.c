#include<stdio.h>
#include<stdlib.h>
#include"lista_encadeada.h"

int main()
{
  tipoNo *l; tipoDado d;
  criar(&l);

  d.id = 0;
  d.valor = 15;

  inserir(&l, d);

  d.id = 1;
  d.valor = 25;
  inserir(&l, d);

  d.id = 2;
  d.valor = 30;
  inserir(&l, d);

  printf("ENDEREÇO DA LISTA: %p\n", &l);
  mostrar(l);

  return 0;
}
