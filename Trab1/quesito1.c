#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include"metodosVetores.h"

int main()
{
  int x, *v, *vBusca, resultBusca;
  float t1;
  clock_t tempo;

  v = (int *)malloc(1000000 * sizeof(int));
  vBusca = (int *)malloc(30*sizeof(int));

  inserirAleatorio(vBusca,30);
  inserirOrdenado(v,1000000);

  float *valoresTemp;
  valoresTemp = (float *)malloc(30*sizeof(float));

  printf("Busca Sequencial\n\n");
  for(int i = 0; i < 30; i++)
  {
    tempo = clock();
    resultBusca = buscaSequencial(v,50,vBusca[i]));
    t1 = medirTempoExec(tempo, clock()));
    printf("Tempo de execução busca %d : %f\n",i, t1);
    valoresTemp[i] = t1;
  }
  printf("Tempo médio de execução da busca sequencial: %f\n", mediaTempoExec(valoresTemp, 30));

  printf("\n\nBusca Binária\n\n");
  for(int i = 0; i < 30; i++)
  {
    tempo = clock();
    resultBusca = buscaSequencial(v,50,vBusca[i]);
    t1 = medirTempoExec(tempo, clock()));
    printf("Tempo de execução busca %d : %f\n",i, t1);
    valoresTemp[i] = t1;
  }
  printf("Tempo médio de execução da busca binária: %f\n", mediaTempoExec(valoresTemp, 30));

  free(valoresTemp);
  free(v);
  free(vBusca);
  return 0;
}
