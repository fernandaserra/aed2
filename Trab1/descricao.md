### Trab1 - Data de entrega : 08/04
##### Algoritmos de busca e ordenação;
##### Vetores e listas.
* Para cada item, deve-se calcular seus tempos de execução e a média deles após todas as operações terem sido feitas.
1. Vetor com 1 milhão de elementos ordenados;
  * Busca binária x Busca sequencial com 30 elementos cada. 
2. Vetor vs lista
3. Vetor com 10000 elementos
4. Ocupação de memória
