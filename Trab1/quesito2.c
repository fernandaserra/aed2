#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include"metodosVetores.h"
#include"lista_encadeada.h"

int main()
{
  int *v, *vBusca, resultBusca;
  float *valoresTemp;
  clock_t tempo; time_t t1;
  tipoNo *l; tipoDado d;

  v = (int *)malloc(15 * sizeof(int));
  vBusca = (int *)malloc(15 * sizeof(int));
  valoresTemp = (float *)malloc(30 * sizeof(float));

  inserirAleatorio(v,15);
  inserirAleatorio(vBusca,30);

  criar(&l);

  for(int i = 0; i < 15; i++)
  {
    d.id = i; d.valor = v[i];
    inserir(&l, d);
  }

  printf("\nBusca sequencial no vetor\n");
  for(int i = 0; i < 30; i++)
  {
    tempo = clock();
    resultBusca = buscaSequencial(v,15,vBusca[i]);
    t1 = medirTempoExec(tempo, clock());
    printf("Tempo de execução busca %d : %f\n",i, t1);
    valoresTemp[i] = t1;
  }
  printf("Tempo médio de execução da busca sequencial no vetor: %f\n", mediaTempoExec(valoresTemp, 30));


  printf("\nBusca sequencial na lista\n");
  for(int i = 0; i < 30; i++)
  {
    tempo = clock();
    printf("INDICE ACHADO: %p\n", buscaLESequencial(l,vBusca[i]));
    t1 = medirTempoExec(tempo, clock());
    printf("Tempo de execução busca %d : %f\n",i, t1);
    valoresTemp[i] = t1;
  }
  printf("Tempo médio de execução da busca sequencial na lista: %f\n", mediaTempoExec(valoresTemp, 30));

  free(valoresTemp);
  free(v);
  free(vBusca);
  free(l);
  return 0;
}
