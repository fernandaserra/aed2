#include"metodosVetores.h"
#include<stdio.h>
#include<stdlib.h>
#include<time.h>

void inserirOrdenado(int v[], int n)
{
  timeo_t t; int x;
  srand((unsigned)time(&t));
  v[0] = x;
  for (int i = 0; i < n; i++)
  {
    x = (rand() % 20);
    v[i] =  v[i - 1] + x;
  }
}

void inserirAleatorio(int v[], int n)
{
  tempo_t t; int x;
  srand((unsigned)time(&t));
  for (int i = 0; i < n; i++) v[i] = (rand() % 20);
}

float medirTempoExec(float t0, float t1)
{
  return ((t1 - t0)/(double)CLOCKS_PER_SEC);
}

float mediaTempoExec(float v[], int n)
{
  float acm = 0;
  for(int i = 0; i < n; i++) acm += v[i];
  return (acm/n);
}

int buscaSequencial(int v[], int n, int chave)
{
  for(int i = 0; i < n; i++)
  {
    if(v[i] == chave) return i;
  }
  return (-1);
}

int buscaBinaria(int v[], int n, int chave)
{
	int inf = 0, sup = (n - 1), meio;
	{
		meio = (inf+sup)/2;
		if(v[meio] == chave) return meio;
    while(inf <= sup)
		if (chave < v[meio]) sup = meio - 1;
		else inf = meio + 1;
	}
	return (-1);
}

void bubbleSort(int v[], int n)
{
  int aux = 0;
  for(int j = 0; j < n - 1; j++)
  {
    for(int i = 0; i < n-1; i++)
    {
      if(v[i] > v[i+1])
      {
        aux = v[i];
        v[i] = v[i + 1];
        v[i + 1] = aux;
      }
    }
  }
}

void trocar(int *x, int *y)
{
    int aux = *x;
    *x = *y;
    *y = aux;
}

int particionar(int v[], int inicio, int fim)
{
    int pivot = v[fim], i = (inicio - 1);
    for (int j = inicio; j <= (fim - 1); j++)
    {
      if (v[j] <= pivot)
      {
        i++;
        trocar(&v[i], &v[j]);
      }
    }
    trocar(&v[i + 1], &v[fim]);
    return (i + 1);
}
void quickSort(int v[], int inicio, int fim)
{
    if (inicio < fim)
    {
      int pi = particionar(v, inicio, fim);
      quickSort(v, inicio, (pi - 1));
      quickSort(v, (pi + 1), fim);
    }
}

void insertionSort(int v[], int n)
{
    int pivot, j;
    for (int i = 1; i < n; i++)
    {
        pivot = v[i]; j = i - 1;
        while ((j >= 0) && (v[j] > pivot))
        {
            v[j + 1] = v[j];
            j = j - 1;
        }
        v[j + 1] = pivot;
    }
}
