#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include"metodosVetores.h"

int main()
{
  int *v; float *t; clock_t tempo;
  v = (int *)malloc(sizeof(int)*100000);
  t = (float *)malloc(sizeof(float) * 30);

  printf("Bubble Sort\n");
  for(int i = 0; i < 30; i++)
  {
    inserirAleatorio(v, 100000);

    tempo = clock();
    bubbleSort(v,100000);
    t[i] = medirTempoExec(tempo, clock());
    printf("Tempo de execução da ordenação[%d]: %f\n",i, t[i]);
  }
  printf("Media tempo exec: %f\n", mediaTempoExec(t,30));

  printf("Insertion Sort\n");
  for(int i = 0; i < 30; i++)
  {
    inserirAleatorio(v, 100000);

    tempo = clock();
    insertionSort(v,100000);
    t[i] = medirTempoExec(tempo, clock());
    printf("Tempo de execução da ordenação[%d]: %f\n",i, t[i]);
  }
  printf("Media tempo exec: %f\n", mediaTempoExec(t,30));

  printf("Quick Sort\n");
  for(int i = 0; i < 30; i++)
  {
    inserirAleatorio(v, 100000);

    tempo = clock();
    quickSort(v,0,100000-1);
    t[i] = medirTempoExec(tempo, clock());
    printf("Tempo de execução da ordenação[%d]: %f\n",i, t[i]);
  }
  printf("Media tempo exec: %f\n", mediaTempoExec(t,30));

  free(v);
  free(t);

  return 0;
}
