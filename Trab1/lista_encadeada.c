#include<stdio.h>
#include<stdlib.h>
#include"lista_encadeada.h"

void criar(tipoNo **p)
{
  *p = NULL;
}

void inserir(tipoNo **l, tipoDado dado)
{
  tipoNo *aux;
  aux = (tipoNo *)malloc(sizeof(tipoNo));
  if(!aux) exit(1);
  aux->d = dado;
  aux->prox = *l;
  *l = aux;
}

void mostrar(tipoNo *l)
{
  while(l != NULL)
  {
    printf("ID: %d\nVALOR: %d\n", l->d.id, l->d.valor);
    printf("ENDEREÇO CÉLULA:%p\n", &(l->prox));
    l = l->prox;
  }
}

tipoNo* buscaLESequencial(tipoNo *l, int chave)
{
  while(l != NULL)
  {
    if (l->d.valor == chave) return l;
    else l = l->prox;
  }
  return NULL;
}
