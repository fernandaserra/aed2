typedef struct tipoDado
{
  int id;
  int valor;
}tipoDado;

typedef struct tipoNo
{
  tipoDado d;
  struct tipoNo *prox;
}tipoNo;

void criar(tipoNo **p);

void inserir(tipoNo **l, tipoDado dado);

void mostrar(tipoNo *l);

tipoNo* buscaLESequencial(tipoNo *l, int chave);
