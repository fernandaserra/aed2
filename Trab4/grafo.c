#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include "grafo.h"

void criar(int numVertices, int M[numVertices][numVertices], float gconectvidade)
{
  time_t t; int x, y;
  srand((unsigned)time(&t));

  int nConexoes = 0;
  for(int i = 0; i < numVertices; i++) nConexoes += i;
  nConexoes *= gconectvidade;
  printf("Número de conexões : %d\n", nConexoes);

  for(int i = 0; i < numVertices; i++)
  {
    for(int j = 0; j < numVertices; j++) M[i][j] = 0;
  }

  for(int i = 0; i < nConexoes; i++)
  {
    x = (rand() % numVertices); y = (rand() % numVertices);
    if(M[x][y] == 1) i -= 1;
    else if (x != y)
    {
      M[x][y] = 1; M[y][x] = 1;
    }
  }
  printf("Número de arestas : %d \n", nConexoes * 2);
}

void mostrarConexoes(int numVertices, int M[numVertices][numVertices])
{
  for(int i = 0; i < numVertices; i++)
  {
    for(int j = 0; j < numVertices; j++) printf("%d ", M[i][j]);
    printf("\n");
  }
}

void caminhamentoLargura(int numVertices, int M[numVertices][numVertices], int linha)
{
  int com = linha;
  for(int i = 0; i < numVertices; i++)
  {
    if((i != 0) && (com == linha)) break;
    else if(M[com][i] == 1)
    {
       com = i;
       //adicionar item visitado em uma lista para mostrar
    }
  }
}
