int* criar(int numVertices, int M[numVertices][numVertices], float gconectvidade);

void mostrarConexoes(int numVertices, int M[numVertices][numVertices]);

void caminhamentoLargura(int numVertices, int M[numVertices][numVertices], int linha, int coluna);

void caminhamentoProfundidade(int numVertices, int M[numVertices][numVertices], int linha, int coluna);
