#include<stdlib.h>
#include<stdio.h>

typedef struct Aluno
{
  int matr, anoIngresso;
  float coefr;
  char nome[50], curso[20];
} Aluno;

void inserirRegistro(Aluno al, FILE *file);

void preencherRegistroAleatoriamente(FILE *file, int n);

void mostrarArquivo(FILE *file);

Aluno buscaSequencialArq(FILE *file, int matr);

//int buscaMaior(FILE *file, int chave, tipoNo **l);

//int buscaMenor(FILE *file, int chave, tipoNo **l)

//int buscaMaiorIgual(FILE *file, int chave, tipoNo **l);

//int buscaMenorIgual(FILE *file, int chave, tipoNo **l);
