#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include"Aluno.h"
#include"abp.h"

tipoNo* criar()
{
	return NULL;
}

tipoNo* inserir(tipoNo *r, int chave)
{
    if(r != NULL)
    {
			if(chave > r->chave) r->dir = inserir(r->dir, chave);
			else r->esq = inserir(r->esq, chave);
    }else
    {
    		r = (tipoNo *)malloc(sizeof(tipoNo));
    		r->chave = chave;
    		r->dir = NULL; r->esq = NULL;
    }
    return r;
}

tipoNo* inserirValAleatorio(tipoNo *r, int n) //insere n valores aleat�rios na �rvore
{
    clock_t t = clock(); srand((unsigned)time(&t));

    for (int i = 0; i < n; i++) r = inserir(r, rand() % 100000);
    return r;
}

int altura(tipoNo *r)
{
   if (!r) return (-1);
   else
   {
      int he = altura(r->esq);
      int hd = altura(r->dir);
      if (he < hd) return (hd + 1);
      else return (he + 1);
   }
}

int fatorBalanceamento(tipoNo *r)
{
  return (altura(r->dir) - altura(r->esq));
}

void camPrefixado(tipoNo *r) //imprime a raiz e depois os menores itens(sub arv da esq) e dps os maiores (sub arv da dir)
{
	if(r != NULL)
	{
		printf("%d ", r->chave);
		camPrefixado(r->esq);
		camPrefixado(r->dir);
	}
}

void camInfixado(tipoNo *r){
    if (r != NULL)
	{
        camInfixado(r->esq);
        printf("%d ", r->chave);
        camInfixado(r->dir);
    }
}

void camPosfixado(tipoNo *r){
    if (r != NULL)
	{
        camPosfixado(r->esq);
        camPosfixado(r->dir);
        printf("%d ", r->chave);
    }
}

tipoNo* buscaAbp(tipoNo *r, int chave)
{
		if (r == NULL) return NULL;
		else if (r->chave == chave) return r;
		else if ((r->chave) > chave) return (buscaAbp(r->esq, chave));
		else if (r->chave < chave) return (buscaAbp(r->dir, chave));
}

tipoNo* converteVet(int v[], int n, tipoNo *r)
{
	r = inserir(r, v[0]);
    for(int i = 1; i < n; i++) r = inserir(r, v[i]);

	return r;
}

tipoNo* drop(tipoNo *r)
{
  if(r)
  {
    drop(r->esq); drop(r->dir);
    free(r);
  }
  return r;
}

void indexar(FILE *file, tipoNo *r)
{
	Aluno al;

	fseek(file, 0, SEEK_SET);
	while(fread(&al, sizeof(Aluno), 1, file))
	{
		inserir(r, al.matr); //falta algum parametro que vá me dizer a posição desse nó no arquivo
	}
}

void buscaMaior(tipoNo *r, int chave, FILE *destino)
{
	if (r == NULL) return ;
	else if (r->chave > chave)
	{
		destino = fopen("./maiores", "a");
		if (!destino) exit(1);

		fprintf(destino, "%d\n", r->chave);
		fclose(destino);
		buscaMaior(r->dir, chave, destino);
	} else if (r->chave < chave) buscaMaior(r->dir, chave, destino);
}

void buscaMaiorIgual(tipoNo *r, int chave, FILE *destino)
{
	if (r == NULL) return ;
	else if (r->chave >= chave)
	{
		destino = fopen("./maiores", "a");
		if (!destino) exit(1);

		fprintf(destino, "%d\n", r->chave);
		fclose(destino);
		buscaMaior(r->dir,  chave, destino);
	} else if (r->chave < chave) buscaMaior(r->dir, chave, destino);
}

void buscaMenor(tipoNo *r, int chave, FILE *destino)
{
	if (r == NULL) return ;
	else if (r->chave < chave)
	{
		destino = fopen("./menores", "a");
		if (!destino) exit(1);

		fprintf(destino, "%d\n", r->chave);
		fclose(destino);
		buscaMaior(r->esq, chave, destino);
	} else if (r->chave > chave) buscaMenor(r->esq, chave, destino);
}

void buscaMenorIgual(tipoNo *r, int chave, FILE *destino)
{
	if (r == NULL) return ;
	else if (r->chave <= chave)
	{
		destino = fopen("./menores", "a");
		if (!destino) exit(1);

		fprintf(destino, "%d\n", r->chave);
		fclose(destino);
		buscaMenor(r->esq, chave, destino);
	} else if (r->chave > chave) buscaMenor(r->esq, chave, destino);
}
