#include"Aluno.h"
//#include"lista_encadeada.h"
#include<stdio.h>
#include<stdlib.h>
#include<time.h>

void inserirRegistro(Aluno al, FILE *file)
{
  fwrite(&al, sizeof(Aluno), 1, file);
}

void preencherRegistroAleatoriamente(FILE *file, int n)
{
  Aluno al[n];
  time_t t; srand((unsigned)time(&t));

  for (int i = 0; i < n; i++)
  {
    al[i].matr = (rand() % n); inserirRegistro(al[i], file);
  }
}

void mostrarArquivo(FILE *file)
{
  fseek(file, 0, SEEK_SET); Aluno al;

  while (fread(&al, sizeof(Aluno), 1, file))
  {
    printf("Matrícula : %d\n", al.matr);
    printf("Nome : %s\n", al.nome);
    printf("CR : %f\n", al.coefr);
    printf("Curso : %s\n", al.curso);
    printf("Ano de Ingresso : %d\n", al.anoIngresso);
    printf("\n");
  }
}

Aluno buscaSequencialArq(FILE *file, int matr)
{
  Aluno al1; fseek(file, 0, SEEK_SET);

  while(fread(&al1, sizeof(Aluno), 1, file))
  {
    if (al1.matr == matr) return al1;
  }

  return al1;
}

int buscaMaior(FILE *file, int chave, tipoNo **l)
{
  tipoDado al; int i = 0, cont = 0;

  while (fread(&al, sizeof(Aluno), 1, file))
  {
    if(al.matr > chave)
    {
      if(i > 4) cont++;
      else
      {
        l->dado = al; inserirLista(l, al);
        i++; cont++;
      }
    }
  }
  return cont;
}

int buscaMenor(FILE *file, int chave, tipoNo **l)
{
  tipoDado al; int i = 0, cont = 0;

  while (fread(&al, sizeof(Aluno), 1, file))
  {
    if(al.matr < chave)
    {
      if(i > 4) cont++;
      else
      {
        l->dado = al; inserirLista(l, al);
        i++; cont++;
      }
    }
  }
  return cont;
}

int buscaMaiorIgual(FILE *file, int chave, tipoNo **l)
{
  tipoDado al; int i = 0, cont = 0;

  while (fread(&al, sizeof(Aluno), 1, file))
  {
    if(al.matr >= chave)
    {
      if(i > 4) cont++;
      else
      {
        l->dado = al; inserirLista(l, al);
        i++; cont++;
      }
    }
  }
  return cont;
}

int buscaMenorIgual(FILE *file, int chave, tipoNo **l)
{
  tipoDado al; int i = 0, cont = 0;

  while (fread(&al, sizeof(Aluno), 1, file))
  {
    if(al.matr <= chave)
    {
      if(i > 4) cont++;
      else
      {
        l->dado = al; inserirLista(l, al);
        i++; cont++;
      }
    }
  }
  return cont;
}

