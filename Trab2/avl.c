#include"avl.h"
#include"abp.h"
#include<stdio.h>
#include<stdlib.h>

tipoNo* criar()
{
  return NULL;
}

tipoNo* inserirBalanceada(tipoNo *r, int chave)
{
  r = inserir(r, chave);
  if(r) return balanceia(r);

  return r;
}

int altura(tipoNo *r) 
{
   if (!r) return (-1);
   else 
   {
      int he = altura(r->esq);
      int hd = altura(r->dir);
      if (he < hd) return (hd + 1);
      else return (he + 1);
   }
}

int fatorBalanceamento(tipoNo *r)
{
  return (altura(r->dir) - altura(r->esq));
}

tipoNo* rotacionarDireita(tipoNo *r)
{
    tipoNo *aux = r, *aux2 = aux->dir;
    aux->dir = aux2->esq;
    aux2->esq = aux;

    return aux2;
}

tipoNo* rotacionarEsquerda(tipoNo *r)
{
  tipoNo *aux = r, *aux2 = aux->esq;
  aux->esq = aux2->dir;
  aux2->dir = aux;

  return aux2;
}

tipoNo* rotacionar2xDireita(tipoNo *r)
{
  r->dir = rotacionarEsquerda(r->dir);
  return rotacionarDireita(r);
}

tipoNo* rotacionar2xEsquerda(tipoNo *r)
{
  r->esq = rotacionarDireita(r->esq);
  return rotacionarEsquerda(r);
}

tipoNo* balancear(tipoNo *r)
{
  tipoNo *rFinal = criar(); int f = fatorBalanceamento(r);

  if(f >= 2) //dir maior
  {
    if (fatorBalanceamento(r->dir) > 1) rFinal = rotacionar2xDireita(r);
    else rFinal = rotacionarDireita(r);
  } else if (f <= -2) //esq maior
  {
    if (fatorBalanceamento(r->esq) < -1) rFinal = rotacionar2xEsquerda(r);
    else rFinal = rotacionarEsquerda(r);
  } else rFinal = r; // não precisa balancear

  return rFinal;
}
