void inserirOrdenado(int v[], int n);

void inserirAleatorio(int v[], int n0, int n1);

int buscaSequencial(int v[], int n, int chave);

int buscaBinaria(int v[], int n, int chave);

void bubbleSort(int v[], int n);

void trocar(int *x, int *y);

int particionar(int v[], int inicio, int fim);

void quickSort(int v[], int inicio, int fim);

void insertionSort(int v[], int n);
