#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include"vetor.h"
#include"abp.h"

int main()
{
    float *valoresTemp;
    clock_t t0, t1;

    tipoNo *arv; arv = criar();
    arv = inserirValAleatorio(arv, 10);

    int *v, *vBusca;
    v = (int *)malloc(sizeof(int)*10); //1M DE ELEMENTOS
    vBusca = (int *)malloc(sizeof(int)*5); //30 ELEMENTOS
    valoresTemp = (float *)malloc(sizeof(float)*5); // 30 ELEMENTOS

    inserirOrdenado(v, 10);
    inserirAleatorio(vBusca, 0, 5);

    //Busca binária no Vetor de 1M
    for(int i = 0; i <= 5; i++)
    {
      t0 = clock();
      buscaBinaria(v,10,vBusca[i]);
      t1 = clock(); valoresTemp[i] = (float) (t1 - t0)/CLOCKS_PER_SEC;

      printf("Tempo de execução busca %d : %f\n",i, valoresTemp[i]);
    }
    float acm = 0;
    for(int i = 0; i < 5; i++) acm += valoresTemp[i];
    printf("Tempo médio de execução da busca binária no vetor: %f\n", acm);

    printf("------------------------------------------------\n");
    //Busca sequencial em árvore ABP
    for(int i = 0; i < 5; i++)
    {
      t0 = clock();
      buscaAbp(arv, vBusca[i]);
      t1 = clock(); valoresTemp[i] = (float) (t1 - t0)/CLOCKS_PER_SEC;

      printf("Tempo de execução busca %d : %f\n",i, valoresTemp[i]);
    }

    acm = 0;
    for(int i = 0; i < 5; i++) acm += valoresTemp[i];
    printf("Tempo médio de execução da busca sequencial na árvore: %f\n", acm);


    free(valoresTemp);
    free(v);
    free(vBusca);

    return 0;
}
