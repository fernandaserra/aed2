#include<stdio.h>
#include<stdlib.h>
#include"abp.h"
#include"vetor.h"

int main()
{
  tipoNo *arv; arv = criar(arv);

  int *v; v = (int *)malloc(sizeof(int) * 30);
  inserirOrdenado(v, 13);
  inserirAleatorio(v, 13, 30);

  printf("----Pacote recebido----\n");
  for(int i = 0; i < 30; i++) printf("%d ", v[i]);

  printf("\n----Pacote montado----\n");
  arv = converteVet(v, 30, arv);
  camInfixado(arv);

  free(v);

  return 0;
}
