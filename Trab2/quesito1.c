#include<stdio.h>
#include<stdlib.h>
#include"abp.h"
int main()
{
	tipoNo *arv; arv = criar();
	arv = inserirValAleatorio(arv, 50);

	printf("Caminhamento Pré-fixado: \n");
	camPrefixado(arv);

	printf("Caminhamento Pós-fixado: \n");
	camPosfixado(arv);

	printf("Caminhamento Infixado: \n");
	camInfixado(arv);

	printf("\n");
	return 0;
}
