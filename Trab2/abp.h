typedef struct tipoNo
{
  int chave;
  struct tipoNo *esq, *dir; //apontadores da esquerda e direita
} tipoNo;

tipoNo* criar();

tipoNo* inserir(tipoNo *r, int chave);

tipoNo* inserirValAleatorio(tipoNo *r, int n);

int altura(tipoNo *r);

int fatorBalanceamento(tipoNo *r);

void camPrefixado(tipoNo *r);

void camPosfixado(tipoNo *r);

void camInfixado(tipoNo *r);

tipoNo* buscaAbp(tipoNo *r, int chave);

tipoNo* converteVet(int v[], int n, tipoNo *r);

tipoNo* drop(tipoNo *r);
