#include<stdlib.h>
#include<stdio.h>

void zerar(int *x)
{
	*x = 0;
}

int main()
{
  int *p, x = 3;
  p = &x;
  printf("%d\n", *p);
  zerar(&x);
  printf("%d\n", *p);
  return 0;
}
