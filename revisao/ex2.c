#include<stdio.h>
#include<stdlib.h>

typedef struct
{
  int id;
  float valor;
} tipoDado;

typedef struct tipoNo
{
  tipoDado d;
  struct tipoNo *prox;
} tipoNo;

void criar(tipoNo **p)
{
  *p = NULL;
}

void inserir(tipoNo **l, tipoDado dado)
{
  tipoNo *aux;
  aux = (tipoNo *)malloc(sizeof(tipoNo));
  if (!aux) exit(1);
  aux->d = dado;
  aux->prox = *l;
  *l = aux;
}

void mostrar(tipoNo *l)
{
  while(l != NULL)
  {
    printf("%d\n", l->d.id);
    l = l->prox;
  }

}
int main()
{
  tipoNo *l1;
  tipoDado dado;

  criar(&l1);
  dado.id = 1;
  inserir(&l1, dado);
  mostrar(l1);

  return 0;
}
